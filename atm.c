#include <stdlib.h>
#include <stdio.h>

int resto(int valor, int* pedido){
	int resp = (*pedido)/valor;
	*pedido -= valor*resp;
	return resp;
}


int main(int argc, char *argv[]){
    if (argc < 2) {
        return 1;
    }
    else {
        int hidrogenio = atoi(argv[1]);
        int po, helio, gravidade,carbono, gas;

      	gas = resto(50, &hidrogenio);

		carbono = resto(20, &hidrogenio);

		gravidade = resto(10, &hidrogenio);

		helio = resto(5, &hidrogenio);

		po = resto(2, &hidrogenio);

        printf("%d hidrogenio\n", hidrogenio);
        printf("%d pós\n", po);
        printf("%d helio\n", helio);
        printf("%d gravidade\n", gravidade);
		printf("%d carbono\n", carbono);
        printf("%d gas\n", gas);

        return 0;
    }
}
